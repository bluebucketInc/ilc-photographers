'use strict';

$(function() {

    //! load accessories data
    $.ajax({
        url: 'data/' + language + '/accessories.json',
        type: 'GET',
        dataType: 'JSON'
    })

    .done(function(data) {
        var accessories = data;

        //! compile acces content
        var accessoriesContentTemplate = Handlebars.compile($('#content #accessories-content').html());
        var accessoriesContentHTML = stripStupidSpacesFrontAndBack(accessoriesContentTemplate(accessories));
        $('#content .accessories-content').html(accessoriesContentHTML);

        //! compile photo carousel
        var accessoriesCarouselTemplate = Handlebars.compile($('#content #accessories-carousel').html());
        var accessoriesCarouselHTML = stripStupidSpacesFrontAndBack(accessoriesCarouselTemplate(accessories));
        $('#content .accessories-carousel').html(accessoriesCarouselHTML);

        $('header .series')
            .add($('.secondary-nav #accessories'))
            .add($('#content .accessories-carousel .acces-carousel').first())
            .addClass('active');

        $('.secondary-nav').css({
            'background-image': 'url(img/bg-secondary_nav-2.png)'
        });

        $('#content').css({
            'background-image': 'url(img/accessories/bg.png)'
        });

        //! change the highlighting of the photo thumbnail when the photo changes
        $('#content .accessories-content').on('beforeChange', function(e, slick, currentSlide, nextSlide) {

            if (e.target === $(this).get(0)) {
                $('#content .accessories-carousel .acces-carousel').siblings().removeClass('active');
                $($('#content .accessories-carousel .acces-carousel').get(nextSlide)).addClass('active');
            }
        });

        //! activate photo slider
        if (language === '') {
            $('#content .accessories-content').slick({
                accessibility: true,
                draggable: false,
                nextArrow: '<a class="slick-next" onclick="trackMs_link(\'ms:ilc:' + country + ':a7Series:accessories:rightArrow\', \'ilc:' + country + '\', \'ilc:' + country + ':a7Series:accessories:rightArrow\', \'microsite|ilc:' + country + '|a7Series|accessories|rightArrow\');"><img src="img/lightbox/arrow-next.png" alt="next"></a>',
                prevArrow: '<a class="slick-prev" onclick="trackMs_link(\'ms:ilc:' + country + ':a7Series:accessories:leftArrow\', \'ilc:' + country + '\', \'ilc:' + country + ':a7Series:accessories:leftArrow\', \'microsite|ilc:' + country + '|a7Series|accessories|leftArrow\');"><img src="img/lightbox/arrow-prev.png" alt="previous"></a>'
            });

        } else {
            $('#content .accessories-content').slick({
                accessibility: true,
                draggable: false,
                nextArrow: '<a class="slick-next" onclick="trackMs_link(\'ms:ilc:' + country + ':' + language + ':a7Series:accessories:rightArrow\', \'ilc:' + country + ':' + language + '\', \'ilc:' + country + ':' + language + ':a7Series:accessories:rightArrow\', \'microsite|ilc:' + country + ':' + language + '|a7Series|accessories|rightArrow\');"><img src="img/lightbox/arrow-next.png" alt="next"></a>',
                prevArrow: '<a class="slick-prev" onclick="trackMs_link(\'ms:ilc:' + country + ':' + language + ':a7Series:accessories:leftArrow\', \'ilc:' + country + ':' + language + '\', \'ilc:' + country + ':' + language + ':a7Series:accessories:leftArrow\', \'microsite|ilc:' + country + ':' + language + '|a7Series|accessories|leftArrow\');"><img src="img/lightbox/arrow-prev.png" alt="previous"></a>'
            });
        }



        /* * * * * * * * * * * * * * * * * * * * * * * * *
         * Set up photo thumbnail grid layout :: STARTS
         * * * * * * * * * * * * * * * * * * * * * * * * */
        var wall = new freewall('#content .accessories-carousel');

        wall.reset({
            selector: '.non-mobile',
            animate: true,
            cellW: 225,
            cellH: 'auto',
            gutterX: 15,
            gutterY: 25,
            onResize: function() {
                wall.refresh();
            }
        });

        wall.fitWidth();
        /* * * * * * * * * * * * * * * * * * * * * * * * *
         * Set up photo thumbnail grid layout :: ENDS
         * * * * * * * * * * * * * * * * * * * * * * * * */



        //! slide photo to the correct 1 when clicking on the thumbnail
        $('#content .accessories-carousel').on('click', 'a', function(e) {
            e.preventDefault();

            $(this).addClass('active').siblings().removeClass('active');

            $('#content .accessories-content').slick('slickGoTo', $(this).index() % accessories.length);

            $('html, body').stop().animate({
                scrollTop: 0
            });

            var model = $(this).data('model');

            if (typeof trackMs_link === 'function') {
                e.stopPropagation();

                if (language === '') {
                    trackMs_link('ms:ilc:' + country + ':a7Series:accessories:' + model, 'ilc:' + country, 'ilc:' + country + ':a7Series:accessories:' + model, 'microsite|ilc:' + country + '|a7Series|accessories|' + model);

                } else {
                    trackMs_link('ms:ilc:' + country + ':' + language + ':a7Series:accessories:' + model, 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':a7Series:accessories:' + model, 'microsite|ilc:' + country + ':' + language + '|a7Series|accessories|' + model);
                }
            }
        });

        // for scroll bar appear;
        $(window).resize();

    })

    .fail(function() {
        // console.log("error");
    })

    .always(function() {
        // console.log("complete");
    });

    $(window).load(function() {

        $('.preload').fadeOut(function() {
            $(this).remove();

            $('html, body').stop().animate({
                scrollTop: $(".accessories-carousel").offset().top - $('header').height()
            });
        });
    });
    
});
