'use strict';

$(function() {
    $('header .stores').addClass('active');

    $(window).resize(function() {
        $('#content .dealers .dealer > a, .dealer .no-links').height('auto');

        var maxHeight = 0;

        $('#content .dealers .dealer').each(function() {

            if ($(this).height() > maxHeight) {
                maxHeight = $(this).height();
            }
        });

        $('#content .dealers .dealer > a, .dealer .no-links').height(maxHeight);
    });

    $(window).resize();

    $(window).load(function() {

        $('.preload').fadeOut(function() {
            $(this).remove();
        });
    });
});
