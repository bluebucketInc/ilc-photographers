'use strict';

$(function() {
    $('header .series').addClass('active');

    $(window).load(function() {

        $('.preload').fadeOut(function() {
            $(this).remove();
        });
    });
});
