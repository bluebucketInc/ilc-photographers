'use strict';

$(function() {

    $.ajaxSetup({
        cache: true
    });

    //! load FB SDK
    $.getScript('//connect.facebook.net/en_US/all.js', function() {

        FB.init({
            appId: config.fbId,
            xfbml: false,
            version: 'v2.3'
        });

    });

    //! load photos data
    $.ajax({
        url: 'data/' + language + '/photos.json',
        type: 'GET',
        dataType: 'JSON'
    })

    .done(function(data) {
        var photos = data;

        //! issue a count id to all photos to reference to the main photo
        _.forEach(photos, function(photo) {
            photo.count = _.indexOf(photos, photo) + 1;
        });

        var country_url = getCountryFromUrl()[0];

        templatingContent();

        $('header .gallery').addClass('active');
        $('.secondary-nav, .secondary-nav-mobile').css({
            'display': 'none'
        });

        $('#content .filters .filter').first().addClass('active');

        //! shuffle the photo thumbnails
        $('#content .photos-carousel .photo-carousel').shuffle();

        var photographer = '';
        var prev_photographer = '';

        var magnificPopupOptions = {
            type: 'inline',
            gallery: {
                enabled: true,
                preload: 0
            },
            closeOnBgClick: true,
            mainClass: 'mfp-fade',
            callbacks: {
                change: function() {
                    if (photographer !== '') {
                        prev_photographer = photographer;
                    }

                    photographer = $(this.content).data('photographer');
                },
                afterChange: function() {
                    this.content.find('img.lazy').lazyload();
                    FB.XFBML.parse(this.content[0]);
                },
                open: function() {
                    this.content.find('img.lazy').lazyload();
                }
            }
        };

        $('#content .photo-carousel-non-mobile').magnificPopup(magnificPopupOptions);
        $('#content .photo-carousel-mobile').magnificPopup(magnificPopupOptions);



        /* * * * * * * * * * * * * * * * * * * * * * * * *
         * Set up photo thumbnail grid layout :: STARTS
         * * * * * * * * * * * * * * * * * * * * * * * * */
        var filter;
        var wall = new freewall('#photos-wall');
        var wallMobile = new freewall('#photos-wall');
        var clear = false;

        wall.reset({
            selector: '.photo-carousel-non-mobile',
            animate: true,
            cellW: 210,
            cellH: 'auto',
            onResize: function() {
                wall.refresh();

                if ($('.filters').hasClass('open') && wall.container.height() < $('.filters').outerHeight()) {
                    wall.container.height($('.filters').outerHeight());
                }
            }
        });

        wallMobile.reset({
            selector: '.photo-carousel-mobile',
            animate: true,
            cellW: 90,
            cellH: 90,
            onResize: function() {
                if ($(window).width() <= 767) {
                    wallMobile.refresh();
                }
            }
        });

        wall.fitWidth();
        wallMobile.fitWidth();
        /* * * * * * * * * * * * * * * * * * * * * * * * *
         * Set up photo thumbnail grid layout :: ENDS
         * * * * * * * * * * * * * * * * * * * * * * * * */



        $('img.lazy').lazyload({
            effect: 'fadeIn'
        });

        //! slide the filters out for desktop
        $('.filters').on('click', 'a.toggle', function(e) {
            e.preventDefault();

            $(this).siblings('.filters-wrapper').stop().slideToggle(function() {
                // console.log($('#photos-wall').height());
                // console.log($('.filters').innerHeight());
                // console.log($('.filters').outerHeight());

                if ($('#photos-wall').height() < $('.filters').outerHeight()) {
                    $('#photos-wall').height($('.filters').outerHeight());
                }
            });
            $(this).find('.up').stop().fadeToggle();
            $(this).find('.down').stop().fadeToggle();

            if ($(this).parent().hasClass('open')) {

                if (typeof trackMs_link === 'function') {

                    if (language === '') {
                        trackMs_link('ms:ilc:' + country + ':gallery:menu:filter:collapse', 'ilc:' + country, 'ilc:' + country + ':gallery:menu:filter:collapse', 'microsite|ilc:' + country + '|gallery|menu|filter|collapse');

                    } else {
                        trackMs_link('ms:ilc:' + country + ':' + language + ':gallery:menu:filter:collapse', 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':gallery:menu:filter:collapse', 'microsite|ilc:' + country + ':' + language + '|gallery|menu|filter|collapse');
                    }
                }

            } else {

                if (typeof trackMs_link === 'function') {

                    if (language === '') {
                        trackMs_link('ms:ilc:' + country + ':gallery:menu:filter:expand', 'ilc:' + country, 'ilc:' + country + ':gallery:menu:filter:expand', 'microsite|ilc:' + country + '|gallery|menu|filter|expand');

                    } else {
                        trackMs_link('ms:ilc:' + country + ':' + language + ':gallery:menu:filter:expand', 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':gallery:menu:filter:expand', 'microsite|ilc:' + country + ':' + language + '|gallery|menu|filter|expand');
                    }
                }
            }

            $(this).parent().stop().toggleClass('open');

            return false;
        });

        //! clear filters
        $('.filters').on('click', 'a.clear', function(e) {
            e.preventDefault();

            clear = true;

            if (typeof trackMs_link === 'function') {

                if (language === '') {
                    trackMs_link('ms:ilc:' + country + ':gallery:menu:filter:clearAll', 'ilc:' + country, 'ilc:' + country + ':gallery:menu:filter:clearAll', 'microsite|ilc:' + country + '|gallery|menu|filter|clearAll');

                } else {
                    trackMs_link('ms:ilc:' + country + ':' + language + ':gallery:menu:filter:clearAll', 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':gallery:menu:filter:clearAll', 'microsite|ilc:' + country + ':' + language + '|gallery|menu|filter|clearAll');
                }
            }

            $(this).parent().find('input').prop('checked', false);
            $('.filters input').first().change();

            return false;
        });

        /* mobile filter :: STARTS*/
        $('#filter').on('change', function(e) {
            var filter = $(e.target).val();

            if (filter === '') {
                return;
            }

            $('.drop-down-divider').removeClass('hidden');
            $('#camera').add($('#category')).add($('#kol')).add($('#othera7users')).val('').addClass('hidden');
            $('#' + filter).removeClass('hidden');
        });

        $('#camera').add($('#category')).add($('#kol')).add($('#othera7users')).on('change', function(e) {
            var filter = $(e.target).val();

            $('.filters input').prop('checked', false);
            $('.filters').find('input:checkbox[value="' + filter + '"]').prop('checked', true).change();
        });
        /* mobile filter :: ENDS*/

        //! filtering magic
        $('.filters').on('change', 'input', function(e) {
            $('.filters').find('input:checkbox[value="' + $(e.target).val() + '"]').prop('checked', $(e.target).prop('checked'));

            var _filtered;

            var type = $(this).prop('name');
            var checked_val = $(this).val();
            var ticked = $(this).is(":checked");

            if (ticked) {

                _filtered = _.filter(data, function(n) {

                    var results;

                    if (type == 'photographer') {
                        results = n['photographer']['nick'] == checked_val;
                    }

                    if (type == 'category') {
                        results = _.includes(n['category'], checked_val);
                    }

                    if (type == 'camera') {
                        results = n['camera']['name'] == checked_val;
                    }

                    return results;
                });

                var filtered_photographers = _.unique(_.pluck(_.flatten(_.pluck(_filtered, 'photographer')), 'nick'));

                var filtered_categories = [];

                if (type != 'category') {
                    filtered_categories = _.unique(_.flatten(_.pluck(_filtered, 'category')));
                }

                var filtered_cameras = _.unique(_.pluck(_.flatten(_.pluck(_filtered, 'camera')), 'name'));

                var filtered_all = _.union(filtered_photographers, filtered_categories, filtered_cameras);

                $.each(filtered_all, function(i, val) {
                    $('#cell-' + val).prop('checked', true);
                });

            } else {

                if (type == 'category') {

                    if ($('input:checkbox[name="category"]:checked').length <= 0) {
                        photos = [];
                    }
                }

                _filtered = _.filter(photos, function(n) {

                    var results;

                    if (type == 'photographer') {
                        results = n['photographer']['nick'] == checked_val;
                    }

                    if (type == 'category') {
                        results = _.every(n['category'], function() {
                            return _.includes(n['category'], checked_val);
                        });
                    }

                    if (type == 'camera') {
                        results = n['camera']['name'] == checked_val;
                    }

                    return !results;
                });

                var filtered_photographers = _.unique(_.pluck(_.flatten(_.pluck(_filtered, 'photographer')), 'nick'));

                var filtered_categories = _.unique(_.flatten(_.pluck(_filtered, 'category')));

                var filtered_cameras = _.unique(_.pluck(_.flatten(_.pluck(_filtered, 'camera')), 'name'));

                var filtered_all = _.union(filtered_photographers, filtered_categories, filtered_cameras);

                $('.filters').find('input:checkbox:checked').each(function() {

                    var exists = _.includes(filtered_all, $(this).val());

                    if (!exists) {
                        $(this).prop('checked', false);
                    }

                });

            }

            var photographers = [];
            $('.filters').find('input:checkbox[name="photographer"]:checked').each(function() {
                photographers.push($(this).val());
            });

            var categories = [];
            $('.filters').find('input:checkbox[name="category"]:checked').each(function() {
                categories.push($(this).val());
            });

            var cameras = [];
            $('.filters').find('input:checkbox[name="camera"]:checked').each(function() {
                cameras.push($(this).val());
            });

            photos = _.filter(data, function(n) {

                var results = (_.some(photographers, function(photographer) {

                    return n['photographer']['nick'] == photographer;

                }) || _.isEmpty(photographers)) && (_.some(categories, function(category) {

                    return _.includes(n['category'], category);

                }) || _.isEmpty(categories)) && (_.some(cameras, function(camera) {
                    
                    return n['camera']['name'] == camera;

                }) || _.isEmpty(cameras));

                return results;
            });

            photos = _.sortByOrder(photos, ['photographer.nick', 'id'], ['asc', 'desc']);

            if ($.isEmptyObject(photos)) {
                $('#content .message').fadeIn();

            } else {
                $('#content .message').fadeOut();
            }

            //! issue a count id to all photos to reference to the main photo
            _.forEach(photos, function(photo) {
                photo.count = _.indexOf(photos, photo) + 1;
            });

            templatingContent();

            //! shuffle the photo thumbnails
            if (clear) {
                $('#content .photos-carousel .photo-carousel').shuffle();
                clear = !clear;
            }

            $('#content .photo-carousel').off('click.magnificPopup').removeData('magnificPopup');

            $('#content .photo-carousel-non-mobile').magnificPopup(magnificPopupOptions);
            $('#content .photo-carousel-mobile').magnificPopup(magnificPopupOptions);

            $('img.lazy').lazyload({
                effect: 'fadeIn'
            });

            $(window).resize();
        });

        $('#content .navbar-toggle').on('click', function(e) {
            e.preventDefault();

            if ($('#content .filters').hasClass('in') === false && $('#content .filters').hasClass('collapsing') === false) {
                $(this).find('.glyphicon').removeClass('glyphicon-menu-down');
                $(this).find('.glyphicon').addClass('glyphicon-menu-up');

            } else {
                $(this).find('.glyphicon').removeClass('glyphicon-menu-up');
                $(this).find('.glyphicon').addClass('glyphicon-menu-down');
            }

        });

        $('body').on('click', 'button.mfp-arrow.mfp-arrow-right.mfp-prevent-close', function() {

            if (typeof trackMs_link === 'function') {

                if (language === '') {
                    trackMs_link('ms:ilc:' + country + ':gallery:' + prev_photographer + ':photo:rightArrow', 'ilc:' + country, 'ilc:' + country + ':gallery:' + prev_photographer + ':photo:rightArrow', 'microsite|ilc:' + country + '|gallery|' + prev_photographer + '|photo|rightArrow');

                } else {
                    trackMs_link('ms:ilc:' + country + ':' + language + ':gallery:' + prev_photographer + ':photo:rightArrow', 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':gallery:' + prev_photographer + ':photo:rightArrow', 'microsite|ilc:' + country + ':' + language + '|gallery|' + prev_photographer + '|photo|rightArrow');
                }
            }
        });

        $('body').on('click', 'button.mfp-arrow.mfp-arrow-left.mfp-prevent-close', function() {

            if (typeof trackMs_link === 'function') {

                if (language === '') {
                    trackMs_link('ms:ilc:' + country + ':gallery:' + prev_photographer + ':photo:leftArrow', 'ilc:' + country, 'ilc:' + country + ':gallery:' + prev_photographer + ':photo:leftArrow', 'microsite|ilc:' + country + '|gallery|' + prev_photographer + '|photo|leftArrow');

                } else {
                    trackMs_link('ms:ilc:' + country + ':' + language + ':gallery:' + prev_photographer + ':photo:leftArrow', 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':gallery:' + prev_photographer + ':photo:leftArrow', 'microsite|ilc:' + country + ':' + language + '|gallery|' + prev_photographer + '|photo|leftArrow');
                }
            }
        });

        $('#content .photos-carousel').on({
            mouseenter: function() {
                var image = $(this).find('.border').siblings('img');

                $(this).find('.border').width(function() {
                    return image.width() - 14;
                });

                $(this).find('.border').height(function() {
                    return image.height() - 14;
                });

                $(this).find('.border').css({
                    'margin-top': '-' + image.height() + 'px'
                });
            },
            mouseleave: function() {}
        }, '.photo-carousel');

        $(window).resize(function() {
            if ($('#photos-wall').height() < $('.filters').outerHeight()) {
                $('#photos-wall').height($('.filters').outerHeight());
            }
        });

        //! for scroll bar appear;
        $(window).resize();

        function templatingContent() {
            try {

                //! compile photo content
                var photosContentTemplate = Handlebars.compile($('#content #photos-content').html());
                var photosContentHTML = stripStupidSpacesFrontAndBack(photosContentTemplate(photos));
                $('#content .photos-content').html(photosContentHTML);

                //! compile photo carousel
                var photosCarouselTemplate = Handlebars.compile($('#content #photos-carousel').html());
                var photosCarouselHTML = stripStupidSpacesFrontAndBack(photosCarouselTemplate(photos));
                $('#content .photos-carousel').html(photosCarouselHTML);

            } catch (e) {
                console.log(e);

                $('#content #photos-content').remove();
                $('#content #photos-carousel').remove();
            }

            if (country_url !== 'sg') {
                $('#content .photo-content .logo a').removeAttr('href').removeAttr('onclick').removeAttr('target').css({
                    'opacity': 1
                });
                $('#content .photo-content .lens a').removeAttr('href').removeAttr('onclick').removeAttr('target').css({
                    'opacity': 1
                });
            }
        }
    })

    .fail(function() {
        // console.log("error");
    })

    .always(function() {
        // console.log("complete");
    });

    $(window).load(function() {

        $('.preload').fadeOut(function() {
            $(this).remove();
        });
    });
    
});
