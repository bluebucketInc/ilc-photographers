'use strict';

$(function() {

    //! images to preload
    var images = [];
    images.push('img/home/main.jpg');
    images.push('img/home/main_2.jpg');

    images.push('img/home/id/benny.jpg');
    images.push('img/home/id/nic.jpg');
    images.push('img/home/ph/pilar.jpg');
    images.push('img/home/my/fakrul.jpg');
    images.push('img/home/th/tavepong.jpg');
    images.push('img/home/sg/bryan.jpg');

    images.push('img/home/th/vin.jpg');
    images.push('img/home/th/dow.jpg');
    images.push('img/home/my/eric.jpg');
    images.push('img/home/sg/darren.jpg');
    images.push('img/home/ph/manny.jpg');
    images.push('img/home/sg/edward.jpg');
    images.push('img/home/th/urasha.jpg');

    images.push('img/home/th/kriengkrai.jpg');
    images.push('img/home/sg/kelvin.jpg');
    images.push('img/home/id/glenn.jpg');
    images.push('img/home/sg/chester.jpg');
    images.push('img/home/sg/aloysius.jpg');
    images.push('img/home/my/nick.jpg');
    images.push('img/home/comingsoon_bg.jpg');

    images.push('img/home/id/benny-mobile.jpg');
    images.push('img/home/id/nic-mobile.jpg');
    images.push('img/home/ph/pilar-mobile.jpg');
    images.push('img/home/my/fakrul-mobile.jpg');
    images.push('img/home/th/tavepong-mobile.jpg');
    images.push('img/home/sg/bryan-mobile.jpg');

    images.push('img/home/th/vin-mobile.jpg');
    images.push('img/home/th/dow-mobile.jpg');
    images.push('img/home/my/eric-mobile.jpg');
    images.push('img/home/sg/darren-mobile.jpg');
    images.push('img/home/ph/manny-mobile.jpg');
    images.push('img/home/sg/edward-mobile.jpg');
    images.push('img/home/th/urasha-mobile.jpg');

    images.push('img/home/th/kriengkrai-mobile.jpg');
    images.push('img/home/sg/kelvin-mobile.jpg');
    images.push('img/home/id/glenn-mobile.jpg');
    images.push('img/home/sg/chester-mobile.jpg');
    images.push('img/home/sg/aloysius-mobile.jpg');
    images.push('img/home/my/nick-mobile.jpg');

    new preLoader(images, {
        onComplete: function(loaded, errors) {

            $('.preload').fadeOut(function() {
                $(this).remove();

                if (splash) {
                    $('#content').css ('overflow-y', 'hidden');

                    TweenLite.delayedCall(1, function() {

                        reveal();

                        var slots = [];

                        for (var i = 0; i < 12; i++) {
                            slots[i] = i + 1;
                        }

                        var switchInt = 18;
                        var randomInt = 0;

                        $('#splash').fadeIn(function() {

                            setInterval(function() {
                                randomInt = getRandomInt(1, 12, switchInt);
                                var random = slots[randomInt - 1];

                                $('#splash .bg-' + random).fadeOut(function() {
                                    $(this).removeClass('bg-' + random).addClass('bg-' + switchInt).fadeIn();

                                    slots[randomInt - 1] = switchInt;
                                    switchInt = random;
                                })

                            }, 3000);

                        });

                        function getRandomInt(min, max, prev) {
                            var result = Math.round(Math.random() * (max - min)) + min;

                            if (result === prev) {
                                return getRandomInt(min, max, prev);
                            }

                            return result;
                        }
                    });

                } else {
                    reveal();
                }
            });
        }
    });

    msieversion();

    var splash;
    var country_url = getCountryFromUrl();

    if (country_url !== null) {
        country_url = country_url[0];
    }

    if ($('#splash').length > 0) {
        // splash = (typeof $.cookie('splash') === 'undefined') ? true : false;
        splash = true;

    } else if (country_url === 'sea' && country_url !== 'undefined') {
        splash = false;

        $('#content .photographer').css({
            'opacity': '0.8'
        });

    } else {
        splash = false;

        $('#content .photographer[data-country=' + country_url + ']').css({
            'opacity': '0.8'
        });
    }

    $('#content .photographer#photographer-more').css({
        'opacity': '0.8'
    });

    $("#country-select").change(function(e) {
        var country_url = $("select option:selected").attr('id').toUpperCase();

        if (typeof trackMs_link === 'function') {
            e.stopPropagation();

            trackMs_link('ms:ilc:landing:country-selector:' + country_url, 'ilc', 'ilc:landing:country-selector:' + country_url, 'microsite|ilc|landing|country-selector|' + country_url);
        }

        window.location.href = $("select option:selected").val();
    })

    function reveal() {

        //! load background image
        var windowWidth = $(window).width();

        function desktopBg() {
            $('#photographer-id-benny .bg.desktop').backstretch('img/home/id/benny.jpg');
            $('#photographer-id-nicoline .bg.desktop').backstretch('img/home/id/nic.jpg');
            $('#photographer-ph-pilar .bg.desktop').backstretch('img/home/ph/pilar.jpg');
            $('#photographer-my-fakrul .bg.desktop').backstretch('img/home/my/fakrul.jpg');
            $('#photographer-th-tavepong .bg.desktop').backstretch('img/home/th/tavepong.jpg');
            $('#photographer-sg-bryan .bg.desktop').backstretch('img/home/sg/bryan.jpg');

            $('#photographer-th-vin .bg.desktop').backstretch('img/home/th/vin.jpg');
            $('#photographer-th-dow .bg.desktop').backstretch('img/home/th/dow.jpg');
            $('#photographer-my-eric .bg.desktop').backstretch('img/home/my/eric.jpg');
            $('#photographer-sg-darren .bg.desktop').backstretch('img/home/sg/darren.jpg');
            $('#photographer-ph-manny .bg.desktop').backstretch('img/home/ph/manny.jpg');
            $('#photographer-sg-edward .bg.desktop').backstretch('img/home/sg/edward.jpg');
            $('#photographer-th-urasha .bg.desktop').backstretch('img/home/th/urasha.jpg');

            $('#photographer-th-kriengkrai .bg.desktop').backstretch('img/home/th/kriengkrai.jpg');
            $('#photographer-sg-kelvin .bg.desktop').backstretch('img/home/sg/kelvin.jpg');
            $('#photographer-id-glenn .bg.desktop').backstretch('img/home/id/glenn.jpg');
            $('#photographer-sg-aloysius .bg.desktop').backstretch('img/home/sg/aloysius.jpg');
            $('#photographer-sg-chester .bg.desktop').backstretch('img/home/sg/chester.jpg');
            $('#photographer-my-nick .bg.desktop').backstretch('img/home/my/nick.jpg');
            $('#photographer-coming-soon .bg.desktop').backstretch('img/home/comingsoon_bg.jpg');
        }

        function mobileBg() {
            $('#photographer-id-benny .bg.mobile').backstretch('img/home/id/benny-mobile.jpg');
            $('#photographer-id-nicoline .bg.mobile').backstretch('img/home/id/nic-mobile.jpg');
            $('#photographer-ph-pilar .bg.mobile').backstretch('img/home/ph/pilar-mobile.jpg');
            $('#photographer-my-fakrul .bg.mobile').backstretch('img/home/my/fakrul-mobile.jpg');
            $('#photographer-th-tavepong .bg.mobile').backstretch('img/home/th/tavepong-mobile.jpg');
            $('#photographer-sg-bryan .bg.mobile').backstretch('img/home/sg/bryan-mobile.jpg');

            $('#photographer-th-vin .bg.mobile').backstretch('img/home/th/vin-mobile.jpg');
            $('#photographer-th-dow .bg.mobile').backstretch('img/home/th/dow-mobile.jpg');
            $('#photographer-my-eric .bg.mobile').backstretch('img/home/my/eric-mobile.jpg');
            $('#photographer-sg-darren .bg.mobile').backstretch('img/home/sg/darren-mobile.jpg');
            $('#photographer-ph-manny .bg.mobile').backstretch('img/home/ph/manny-mobile.jpg');
            $('#photographer-sg-edward .bg.mobile').backstretch('img/home/sg/edward-mobile.jpg');
            $('#photographer-th-urasha .bg.mobile').backstretch('img/home/th/urasha-mobile.jpg');

            $('#photographer-th-kriengkrai .bg.mobile').backstretch('img/home/th/kriengkrai-mobile.jpg');
            $('#photographer-sg-kelvin .bg.mobile').backstretch('img/home/sg/kelvin-mobile.jpg');
            $('#photographer-id-glenn .bg.mobile').backstretch('img/home/id/glenn-mobile.jpg');
            $('#photographer-sg-aloysius .bg.mobile').backstretch('img/home/sg/aloysius-mobile.jpg');
            $('#photographer-sg-chester .bg.mobile').backstretch('img/home/sg/chester-mobile.jpg');
            $('#photographer-my-nick .bg.mobile').backstretch('img/home/my/nick-mobile.jpg');
            
        }

        /*if (windowWidth <= 768) {
            mobileBg();

        } else {
            desktopBg();

            if (getInternetExplorerVersion() === 8) {
                setTimeout(function() {
                    desktopBg();
                }, 100);
            }
        }*/

        mobileBg();
        desktopBg();

        if (typeof country_url !== 'undefined') {

            // if (country_url !== 'sg') {
                $('#home-main .bg.desktop').backstretch('img/home/main_2.jpg');

                if (getInternetExplorerVersion() === 8) {
                    setTimeout(function() {
                        $('#home-main .bg.desktop').backstretch('img/home/main_2.jpg');
                    }, 100);
                }

            // } else if (country_url === 'sg') {
            //     $('#home-main .bg.desktop').backstretch('img/home/main.jpg');

            //     if (getInternetExplorerVersion() === 8) {
            //         setTimeout(function() {
            //             $('#home-main .bg.desktop').backstretch('img/home/main.jpg');
            //         }, 100);
            //     }
            // }
        }

        $('header')
            .add($('#content'))
            .add($('footer'))
            .fadeIn();

        $('header .home').addClass('active');

        $(window).resize();

        $(window).load(function() {
            $("#country-select option:eq(0)").prop('selected', true);
        })
    }

    function msieversion() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
            // If Internet Explorer, return version number
            $('#content .photographer').css({
                'border': '1px solid transparent',
            });

        } else {
            // If another browser, return 0
        }

        return false;
    }
});
