'use strict';

$(function() {
    $('header .other-a7users').addClass('active');


    Handlebars.registerHelper('othera7usersRenameTracking', function(text) {
        return text.toLowerCase().replace('&trade;', '').replace(/[^\w\d\s]+/g, '').replace(/\ +/g, '');
    });



    $.ajaxSetup({
        cache: true
    });

    //! load videos data
    $.ajax({
        url: 'data/' + language + '/other-a7users.json',
        type: 'GET',
        dataType: 'JSON'
    })
    .done(function(data) { 

        console.log("success");
        
        var othera7users = data;

        templatingContent();

        function templatingContent() {
            try {

                //! compile filter content
                var othera7usersContent = Handlebars.compile($('#content #othera7users-content').html());
                var othera7usersContentHTML = stripStupidSpacesFrontAndBack(othera7usersContent(othera7users));
                $('#content .othera7users-content').html(othera7usersContentHTML);

                // makeTextBoxesSameHeight();

            } catch (e) {
                console.log(e);
                $('#content #othera7users-content').remove();
            }
            $('img.lazy').lazyload({
                effect: 'fadeIn'
            });
            // $(window).lazyLoadXT();
        }
        // $(window).resize();
        $(window).resize(function(){
            makeTextBoxesSameHeight();
        })

    })
    .fail(function(e) {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });



    // makeTextBoxesSameHeight
    function makeTextBoxesSameHeight() {
 
        var maxheight = 0;
 
        $(".box-wrap .genre").css('height','auto');
 
        $(".box-wrap .genre").each(function() {
            if ($(this).height() > maxheight) {
                maxheight = $(this).height();
            }
        });
 
        $(".box-wrap .genre").height(maxheight + 2);
    }




    // $(window).resize();

    $(window).load(function() {

        $('.preload').fadeOut(function() {
            $(this).remove();
        });

        makeTextBoxesSameHeight();
    });



});
