'use strict';

$(function() {

    $('header .other-a7users').addClass('active');
    $('.photo-carousel-title').addClass('hidden');

    $.ajaxSetup({
        cache: true
    });

    //! load FB SDK
    $.getScript('//connect.facebook.net/en_UK/all.js', function() {

        FB.init({
            appId: config.fbId,
            xfbml: false,
            version: 'v2.3'
        });

    });

    //! load the photos data
    $.ajax({
        url: 'data/' + language + '/photos.json',
        type: 'GET',
        dataType: 'JSON'
    })

    .done(function(data) {

        //! get the photographer ID
        var photographerId = getUrlVar('p');
        var photographerProfile = getProfile();
        var photographerCountry = getPhotographerCountry(photographerProfile);
        var photos;
        var photographer;
        // var photographerGenre;

        //! filter out the photographer's photos
        photos = _.filter(data, function(n) {
            return n.photographer.nick == photographerProfile;
        });

        photos = _.sortByOrder(photos, ['id'], ['desc']);

        // photographerGenre = _.chain(photos).take(1).pluck('genre').first().value();
        // console.log(photographerGenre);

        photographer = _.chain(photos).take(1).pluck('photographer').pluck('nick').first().value();

        try {

            //! compile photo content
            var photosContentTemplate = Handlebars.compile($('#content #photos-content').html());
            var photosContentHTML = stripStupidSpacesFrontAndBack(photosContentTemplate(photos));
            $('#content .photos-content').html(photosContentHTML);

            //! compile photo carousel - desktop
            var photosCarouselTemplate = Handlebars.compile($('#content #photos-carousel').html());
            var photosCarouselHTML = stripStupidSpacesFrontAndBack(photosCarouselTemplate(photos));
            $('#content .photos-carousel').html(photosCarouselHTML);

            //! compile photo carousel - Mobile
            var photosCarouselTemplate = Handlebars.compile($('#content #photos-carousel-mobile').html());
            var photosCarouselHTML = stripStupidSpacesFrontAndBack(photosCarouselTemplate(photos));
            $('#content .photos-carousel-mobile').html(photosCarouselHTML);

        } catch (e) {
            // console.log(e);

            $('#content #photos-content').remove();
            $('#content #photos-carousel').remove();
            $('#content #photos-carousel-mobile').remove();
        }

        $('img.lazy').lazyload({
            effect: 'fadeIn'
        });

        //! load background image
        var windowWidth = $(window).width();

        //! mobile
        if (windowWidth <= (768 - 1)) {
            $('#content').css({
                'background-image': 'url(img/profile/' + photographerCountry + '/' + photographerProfile + '/bg-mobile.png)'
            });

        //     //! tablet
        } else if ((windowWidth >= 768) && (windowWidth <= (992 - 1))) {
            $('#content').css({
                'background-image': 'url(img/profile/' + photographerCountry + '/' + photographerProfile + '/bg-tablet.png)'
            });

        //     //! desktop
        } else {
            $('#content').css({
                // 'background-image': 'url(img/profile/' + photographerCountry + '/' + photographerProfile + '/bg.png)'
                'background-image': 'url(img/bg-gallery.jpg)'
            });
        }

        // $('.secondary-nav').css({
        //     'background-image': 'url(img/profile/' + photographerCountry + '/bg-secondary_nav.png)'
        // });

        // $('.secondary-nav').find('#' + photographerProfile).addClass('active');
        // $('.secondary-nav .nav_countries').find('#' + photographerCountry).addClass('active');

        var country_url = getCountryFromUrl()[0];

        if (country_url !== 'sg') {
            $('#content .photo-content .logo a').removeAttr('href').removeAttr('onclick').removeAttr('target').css({
                'opacity': 1
            });
            $('#content .photo-content .lens a').removeAttr('href').removeAttr('onclick').removeAttr('target').css({
                'opacity': 1
            });
        }

        //! activate carousel
        $('#content .photos-carousel').slick({
            slidesToShow: 6,
            nextArrow: '<a class="slick-next"><img src="img/carousel-next.png"></a>',
            prevArrow: '<a class="slick-prev"><img src="img/carousel-prev.png"></a>',
            lazyLoad: 'ondemand'
        });

        $('#content .photo-carousel, #content .photo-carousel-mobile').magnificPopup({
            type: 'inline',
            gallery: {
                enabled: true,
                preload: [0, 2]
            },
            closeOnBgClick: true,
            mainClass: 'mfp-fade',
            callbacks: {
                change: function() {
                    var popupContent = this.content;
                    setTimeout(function() {
                        $('img.lazy').lazyload() /*.trigger('')*/ ;
                        FB.XFBML.parse(document.getElementById(popupContent[0].id));
                    }, 100);

                }
            }
        });

        $("#kol-select").change(function(e) {
            var country_url = $("select#kol-select option:selected").attr('id').toUpperCase();

            if (typeof trackMs_link === 'function') {
                e.stopPropagation();

                trackMs_link('ms:ilc:landing:country-selector:' + country_url, 'ilc', 'ilc:landing:country-selector:' + country_url, 'microsite|ilc|landing|country-selector|' + country_url);
            }

            window.location.href = $("select#kol-select option:selected").val();
        });

        $('body').on('click', 'button.mfp-arrow.mfp-arrow-right.mfp-prevent-close', function() {

            if (typeof trackMs_link === 'function') {

                if (language === '') {
                    trackMs_link('ms:ilc:' + country + ':profile:' + photographerProfile + ':photo:rightArrow', 'ilc:' + country, 'ilc:' + country + ':profile:' + photographerProfile + ':photo:rightArrow', 'microsite|ilc:' + country + '|profile|' + photographerProfile + '|photo|rightArrow');

                } else {
                    trackMs_link('ms:ilc:' + country + ':' + language + ':profile:' + photographerProfile + ':photo:rightArrow', 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':profile:' + photographerProfile + ':photo:rightArrow', 'microsite|ilc:' + country + ':' + language + '|profile|' + photographerProfile + '|photo|rightArrow');

                }
            }
        });

        $('body').on('click', 'button.mfp-arrow.mfp-arrow-left.mfp-prevent-close', function() {

            if (typeof trackMs_link === 'function') {

                if (language === '') {
                    trackMs_link('ms:ilc:' + country + ':profile:' + photographerProfile + ':photo:leftArrow', 'ilc:' + country, 'ilc:' + country + ':profile:' + photographerProfile + ':photo:leftArrow', 'microsite|ilc:' + country + '|profile|' + photographerProfile + '|photo|leftArrow');

                } else {
                    trackMs_link('ms:ilc:' + country + ':' + language + ':profile:' + photographerProfile + ':photo:leftArrow', 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':profile:' + photographerProfile + ':photo:leftArrow', 'microsite|ilc:' + country + ':' + language + '|profile|' + photographerProfile + '|photo|leftArrow');

                }
            }
        });

        $('.back-to-top').on('click', function() {

            $('html, body').stop().animate({
                scrollTop: 0
            });
        })

    })

    .fail(function() {
        // console.log("error");
    })

    .always(function() {
        // console.log("complete");
    });

    $(window).load(function() {
        $("#kol-select option:eq(0)").prop('selected', true);

        $('.preload').fadeOut(function() {
            $(this).remove();
        });
    });
    
});
