'use strict';

$(function() {
    $('header .reviews').addClass('active');
    $(".language").hide();

    $(window).load(function() {

        $('.preload').fadeOut(function() {
            $(this).remove();
        });
    });
});
