'use strict';

$(function() {

    $.ajaxSetup({
        cache: true
    });

    //! load FB SDK
    $.getScript('//connect.facebook.net/en_UK/all.js', function() {

        FB.init({
            appId: config.fbId,
            xfbml: true,
            version: 'v2.3'
        });

    });

    var _photographerProfile = getProfileFromPhoto();
    var _photographerCountry = getPhotographerCountry(_photographerProfile);

    // console.log(_photographerCountry)

    if (_photographerCountry !== "other") {

        $('.secondary-nav').css({
            'background-image': 'url(img/profile/' + _photographerCountry + '/bg-secondary_nav.png)'
        });

        $('.secondary-nav').find('#' + _photographerProfile).addClass('active');
        $('.secondary-nav .nav_countries').find('#' + _photographerCountry).addClass('active');

        $('.secondary-nav .nav_middle').addClass('show').siblings('.' + _photographerCountry).removeClass('hidden');

        var country_url = getCountryFromUrl()[0];

        if (country_url !== 'sg') {
            $('#content .photo .logo a').removeAttr('href').removeAttr('onclick').removeAttr('target').css({
                'opacity': 1
            });
        }

    } else {
        $('.secondary-nav').addClass("hidden")
        $('#photo #content h1').css({
            'margin-top': '90px'
        });
    }
    

    $(window).load(function() {

        $('.preload').fadeOut(function() {
            $(this).remove();
        });
    });
});
