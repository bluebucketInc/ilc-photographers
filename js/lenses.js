'use strict';

$(function() {

    //! load photos data
    $.ajax({
        url: 'data/' + language + '/lenses.json',
        type: 'GET',
        dataType: 'JSON'
    })

    .done(function(data) {
        var lenses = data;

        _.forEach(lenses, function(lens) {
            lens.id = _.indexOf(lenses, lens) + 1;
        });

        //! compile lens content
        var lensesContentTemplate = Handlebars.compile($('#content #lenses-content').html());
        var lensesContentHTML = stripStupidSpacesFrontAndBack(lensesContentTemplate(lenses));
        $('#content .lenses-content').html(lensesContentHTML);

        //! compile photo carousel
        var lensesCarouselTemplate = Handlebars.compile($('#content #lenses-carousel').html());
        var lensesCarouselHTML = stripStupidSpacesFrontAndBack(lensesCarouselTemplate(lenses));
        $('#content .lenses-carousel').html(lensesCarouselHTML);

        $('header .series')
            .add($('.secondary-nav #lenses'))
            .add($('#content .lenses-carousel .lens-carousel').first())
            .addClass('active');

        $('.secondary-nav').css({
            'background-image': 'url(img/bg-secondary_nav-2.png)'
        });

        $('#content .bg').css({
            'background-image': 'url(' + lenses[0].background + ')'
        });

        //! change the highlighting of the photo thumbnail when the photo changes
        $('#content .lenses-content').on('beforeChange', function(e, slick, currentSlide, nextSlide) {

            if (e.target === $(this).get(0)) {
                $('#content .lenses-carousel .lens-carousel').siblings().removeClass('active');
                $($('#content .lenses-carousel .lens-carousel').get(nextSlide)).addClass('active');

                $('#content .bg').stop().fadeOut(function() {
                    $('#content .bg').css({
                        'background-image': 'url(' + lenses[nextSlide].background + ')'
                    }).fadeIn();
                });
            }
        });

        if (language === '') {

            $('#content .sample').slick({
                slidesToShow: 6,
                draggable: false,
                nextArrow: '<a class="slick-next" onclick="trackMs_link(\'ms:ilc:' + country + ':a7Series:lenses:sampleRightArrow\', \'ilc:' + country + '\', \'ilc:' + country + ':a7Series:lenses:sampleRightArrow\', \'microsite|ilc:' + country + '|a7Series|lenses|sampleRightArrow\')"><img src="img/carousel-next.png"></a>',
                prevArrow: '<a class="slick-prev" onclick="trackMs_link(\'ms:ilc:' + country + ':a7Series:lenses:sampleLeftArrow\', \'ilc:' + country + '\', \'ilc:' + country + ':a7Series:lenses:sampleLeftArrow\', \'microsite|ilc:' + country + '|a7Series|lenses|sampleLeftArrow\')"><img src="img/carousel-prev.png"></a>'
            });

            //! activate photo slider
            $('#content .lenses-content').slick({
                draggable: false,
                adaptiveHeight: true,
                nextArrow: '<a class="slick-next" onclick="trackMs_link(\'ms:ilc:' + country + ':a7Series:lenses:rightArrow\', \'ilc:' + country + '\', \'ilc:' + country + ':a7Series:lenses:rightArrow\', \'microsite|ilc:' + country + '|a7Series|lenses|rightArrow\')"><img src="img/lightbox/arrow-next.png"></a>',
                prevArrow: '<a class="slick-prev" onclick="trackMs_link(\'ms:ilc:' + country + ':a7Series:lenses:leftArrow\', \'ilc:' + country + '\', \'ilc:' + country + ':a7Series:lenses:leftArrow\', \'microsite|ilc:' + country + '|a7Series|lenses|leftArrow\');"><img src="img/lightbox/arrow-prev.png"></a>'
            });

        } else {
            $('#content .sample').slick({
                slidesToShow: 6,
                draggable: false,
                nextArrow: '<a class="slick-next" onclick="trackMs_link(\'ms:ilc:' + country + ':' + language + ':a7Series:lenses:sampleRightArrow\', \'ilc:' + country + ':' + language + '\', \'ilc:' + country + ':' + language + ':a7Series:lenses:sampleRightArrow\', \'microsite|ilc:' + country + ':' + language + '|a7Series|lenses|sampleRightArrow\')"><img src="img/carousel-next.png"></a>',
                prevArrow: '<a class="slick-prev" onclick="trackMs_link(\'ms:ilc:' + country + ':' + language + ':a7Series:lenses:sampleLeftArrow\', \'ilc:' + country + ':' + language + '\', \'ilc:' + country + ':' + language + ':a7Series:lenses:sampleLeftArrow\', \'microsite|ilc:' + country + ':' + language + '|a7Series|lenses|sampleLeftArrow\')"><img src="img/carousel-prev.png"></a>'
            });

            //! activate photo slider
            $('#content .lenses-content').slick({
                draggable: false,
                adaptiveHeight: true,
                nextArrow: '<a class="slick-next" onclick="trackMs_link(\'ms:ilc:' + country + ':' + language + ':a7Series:lenses:rightArrow\', \'ilc:' + country + ':' + language + '\', \'ilc:' + country + ':' + language + ':a7Series:lenses:rightArrow\', \'microsite|ilc:' + country + ':' + language + '|a7Series|lenses|rightArrow\')"><img src="img/lightbox/arrow-next.png"></a>',
                prevArrow: '<a class="slick-prev" onclick="trackMs_link(\'ms:ilc:' + country + ':' + language + ':a7Series:lenses:leftArrow\', \'ilc:' + country + ':' + language + '\', \'ilc:' + country + ':' + language + ':a7Series:lenses:leftArrow\', \'microsite|ilc:' + country + ':' + language + '|a7Series|lenses|leftArrow\');"><img src="img/lightbox/arrow-prev.png"></a>'
            });
        }

        $('.sample, .sample-mobile').each(function() { //! the containers for all your galleries

            $(this).magnificPopup({
                delegate: '.sample-image', //! the selector for gallery item
                type: 'image',
                gallery: {
                    enabled: true,
                    preload: [0, 2]
                },
                showCloseBtn: true,
                closeBtnInside: false,
                closeOnBgClick: true,
                mainClass: 'mfp-with-zoom',
                zoom: {
                    enabled: true, //! By default it's false, so don't forget to enable it

                    duration: 300, //! duration of the effect, in milliseconds
                    easing: 'ease-in-out', //! CSS transition easing function

                    //! The "opener" function should return the element from which popup will be zoomed in
                    //! and to which popup will be scaled down
                    //! By defailt it looks for an image tag:
                    opener: function(openerElement) {
                        //! openerElement is the element on which popup was initialized, in this case its <a> tag
                        //! you don't need to add "opener" option if this code matches your needs, it's defailt one.
                        return openerElement.is('img') ? openerElement : openerElement.find('img');
                    }
                }
            });
        });



        /* * * * * * * * * * * * * * * * * * * * * * * * *
         * Set up photo thumbnail grid layout :: STARTS
         * * * * * * * * * * * * * * * * * * * * * * * * */
        var wall = new freewall('#content .lenses-carousel');

        wall.reset({
            // selector: '.lens-carousel',
            selector: '.non-mobile',
            animate: true,
            cellW: 225,
            cellH: 'auto',
            gutterX: 15,
            gutterY: 25,
            onResize: function() {
                wall.refresh();
            }
        });

        wall.fitWidth();
        /* * * * * * * * * * * * * * * * * * * * * * * * *
         * Set up photo thumbnail grid layout :: ENDS
         * * * * * * * * * * * * * * * * * * * * * * * * */



        //! slide photo to the correct 1 when clicking on the thumbnail
        $('#content .lenses-carousel').on('click', 'a', function(e) {
            e.preventDefault();

            $(this).addClass('active').siblings().removeClass('active');

            $('#content .bg').stop().fadeOut(function() {
                $('#content .bg').css({
                    'background-image': 'url(' + lenses[$(this).index() % lenses.length].background + ')'
                }).fadeIn();
            });

            $('#content .lenses-content').slick('slickGoTo', $(this).index() % lenses.length);
            $($('#content .lenses-content .lens-content').get($(this).index() % lenses.length + 1)).find('.sample').slick('slickGoTo', 0);

            $('html, body').stop().animate({
                scrollTop: 0
            });

            var model = $(this).data('model');

            if (typeof trackMs_link === 'function') {
                e.stopPropagation();

                if (language === '') {
                    trackMs_link('ms:ilc:' + country + ':a7Series:lenses:' + model, 'ilc:' + country, 'ilc:' + country + ':a7Series:lenses:' + model, 'microsite|ilc:' + country + '|a7Series|lenses|' + model);

                } else {
                    trackMs_link('ms:ilc:' + country + ':' + language + ':a7Series:lenses:' + model, 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':a7Series:lenses:' + model, 'microsite|ilc:' + country + ':' + language + '|a7Series|lenses|' + model);
                }
            }
        });

        $('#content .sample').on('click', 'a', function(e) {
            e.preventDefault();

            var model = $(this).data('model');

            if (typeof trackMs_link === 'function') {
                e.stopPropagation();

                if (language === '') {
                    trackMs_link('ms:ilc:' + country + ':a7Series:lenses:' + model + ':sample', 'ilc:' + country, 'ilc:' + country + ':a7Series:lenses:' + model + ':sample', 'microsite|ilc:' + country + '|a7Series|lenses|' + model + '|sample');

                } else {
                    trackMs_link('ms:ilc:' + country + ':' + language + ':a7Series:lenses:' + model + ':sample', 'ilc:' + country + ':' + language, 'ilc:' + country + ':' + language + ':a7Series:lenses:' + model + ':sample', 'microsite|ilc:' + country + ':' + language + '|a7Series|lenses|' + model + '|sample');
                }
            }
        });

        // for scroll bar appear;
        $(window).resize();
    })

    .fail(function() {
        // console.log("error");
    })

    .always(function() {
        // console.log("complete");
    });


    $(window).load(function() {

        $('.preload').fadeOut(function() {
            $(this).remove();

            $('html, body').stop().animate({
                scrollTop: $(".lenses-carousel").offset().top - 61
            });
        });
    });

    
});
