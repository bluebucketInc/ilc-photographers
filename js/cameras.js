'use strict';

$(function() {
    $('header .series')
        .add($('.secondary-nav #cameras'))
        .addClass('active');

    $('.secondary-nav').css({
        'background-image': 'url(img/bg-secondary_nav-2.png)'
    });

    $(window).load(function() {

        $('.preload').fadeOut(function() {
            $(this).remove();
        });
    });
});
