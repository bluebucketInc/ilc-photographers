Profile
========================  

# Adding a new KOL profile


## Image  
  
1. Image is located at `img/profile/`.
2. Create a folder under respective country with the photographer's name.
  
  

## HTML  
  
1. Creating a template.html files with the photographer's name in short form in all the countries folder. (i.e. `/id/profile-benny.template.html`)
2. Update the content accordingly. Example of things to edit:
	- title
	- "photographerCountry": "`sg`" 
	-  @@include ('../template/content-profile-`sg-bryan.html`', { "country": "`sg`" })
	- parameter for the ommniture tracking code. (i.e. "s.pageName"     : "ms:ilc:sg:profile:`SG:bryan`")
3. Do this for the other 2 languages. (i.e. `/id/thai/profile-benny.template.html`)

  
***Please note the template.html file for 3 languages will have slight coding different, for example, the path, and the additional languages linking, please don't just duplicate over.***  
  
  
  
## Profile Content  
  
The content of each profile is located at `template`. (i.e. `template/content-profile-id-benny.html`)  

This is where the place you can add or edit the  

1. title  
2. desriptions  
3. copies  
4. quote  
5. Interview Q&A
  
  
After completed the content in english version, do this for the other 2 languages.(i.e. `template/thai/content-profile-id-benny.html`)  

***Please note the html file for 3 languages will have slight coding different, please don't just duplicate over.***  