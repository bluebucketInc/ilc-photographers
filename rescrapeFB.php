<?php

class Photographer {
	public $name;
	public $num_photos;

	function __construct ($name, $num_photos) {
		$this -> name = $name;
		$this -> num_photos = $num_photos;
	}
}

include 'FacebookDebugger.php';
$fb = new FacebookDebugger ();

$domain = 'http://sony-asia-new.dev.interuptive.com/ilc/photographers/';
// $domain = 'http://www.sony-asia.com/microsite/ilc/photographers/';
$countries = ['id/', 'my/', 'ph/', 'sea/', 'sg/', 'th/', 'vn/'];
$languages = ['', 'thai/', 'viet/'];

$photographers = [];
array_push($photographers, new Photographer ('benny', 12));
array_push($photographers, new Photographer ('bryan', 34));
array_push($photographers, new Photographer ('chester', 24));
array_push($photographers, new Photographer ('darren', 27));
array_push($photographers, new Photographer ('edward', 33));
array_push($photographers, new Photographer ('eric', 24));
array_push($photographers, new Photographer ('fakrul', 37));
array_push($photographers, new Photographer ('glenn', 6));
array_push($photographers, new Photographer ('kelvin', 21));
array_push($photographers, new Photographer ('kriengkrai', 5));
array_push($photographers, new Photographer ('manny', 16));
array_push($photographers, new Photographer ('nick', 14));
array_push($photographers, new Photographer ('nicoline', 6));
array_push($photographers, new Photographer ('pilar', 16));
array_push($photographers, new Photographer ('tavepong', 20));
array_push($photographers, new Photographer ('urasha', 7));
array_push($photographers, new Photographer ('vin', 9));
array_push($photographers, new Photographer ('brendan', 2));
array_push($photographers, new Photographer ('aloysius', 4));
array_push($photographers, new Photographer ('daniel', 10));
array_push($photographers, new Photographer ('ericchen', 7));
array_push($photographers, new Photographer ('joyce', 10));

$space = "\r\n<br />";

$count = 0;

// echo $domain . $space;
$fb -> reload ($domain);
$count ++;

foreach ($countries as $country) {
	// echo $domain . $country . $space;
	$fb -> reload ($domain . $country);
	$count ++;

	foreach ($languages as $language) {

		if ($language !== '') {
			// echo $domain . $country . $language . $space;
			$fb -> reload ($domain . $country . $language);
			$count ++;
		}

		foreach ($photographers as $photographer) {

			for ($i = 1; $i <= $photographer -> num_photos; $i ++) {
				// echo $domain . $country . $language . 'photos/photo-' . $photographer -> name . '-' . $i . '.html' . $space;
				$fb -> reload ($domain . $country . $language . 'photos/photo-' . $photographer -> name . '-' . $i . '.html');
				$count ++;
			}
		}
	}
}

// echo 'Total number of links: ' . $count;
file_put_contents ('log.txt', 'Total number of links: ' . $count, FILE_APPEND);

?>