Photo
========================  

* Image is located at `img/profile/`.
* The data of the photos will be shared among with `profile's carousel`, `individual Facebook share photos`, and `gallery`. 
* Individual Facebook share photos is the page that user will be able to access from Facebook when they click on someones shared photos link.
* Individual Facebook share photos will only be generated in dist folder. (i.e. `\dist\id\photos\photo-aloysius-3.html`)


  

# Data  

Every time when there's new photo to be added, there are 3 JSON files to be updated, which is English, Thai, and Viet.  

Data is located at `data/photos.json`, `data/thai/photos.json` and `data/viet/photos.json`.  


Way of filling the data  

1. id : increasing as per every new additional photo for each KOL  
2. country : photographer's country code  
3. category : this will display on Gallery filter  
4. title : title of the photo  
5. description : desription of the photo  
6. camera : Can be reused, unless there's a new one to be added.
	* name : model name in full caps
	* alt : Camera name
	* logo : image path of the logo
	* link : camera product sg URL
7. lens : _Optional_. Can be reused, unless there's a new one to be added.  
	* model : model name in full caps  
	* specs : lenses specs  
	* img : image path of the lens  
	* link : lens product sg URL  
8. lens2 : _Optional_  
9. photographer  
	* name : photographer name  
	* nick : normally is the short form of the photographer name  
	* country : full world spelled of photographer's country  
10. specs : specs of the photo  


  


## Pop up  
  
* There is 2 pop up templates, one is SG version, the rest are sharing the same version.
* For SG version, the lenses and Camera are clickable and will link out to respectively URL, while the rest countries are not. 
* To update the pop up template, you have to edit these 3 files `content-photo.html`, `content-profile-photo-carousel.html` and `content-gallery.html` for 3 languages.
