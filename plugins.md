Plugins 
========================  


## [Bootstrap](http://getbootstrap.com/)

Use for developing responsive web.

## [Handlebars.js](http://handlebarsjs.com/)

Template library to compile photos.

## [lodash](https://lodash.com/)

JS utility library.

## [slick](http://kenwheeler.github.io/slick/)

Use for carousel.

## [Magnific Popup](http://dimsemenov.com/plugins/magnific-popup/)

Use for lightbox.

## [Freewall](http://vnjs.net/www/project/freewall/)

Use for the grid layout.

## [Backstretch](http://srobbin.com/jquery-plugins/backstretch/)

Use to stretch image as background image. Mainly to tackle IE8 issue.

## [GreenSock GSAP](http://greensock.com/gsap)

Use for animation.

## ~~[jQuery Cookie](https://github.com/carhartl/jquery-cookie)~~

~~Use for setting cookie.~~

## ~~[Modernizr](http://modernizr.com/)~~

~~Use for detecting HTML5 and CSS3 features.~~