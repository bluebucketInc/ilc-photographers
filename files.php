<?php

class Photographer {
	public $name;
	public $num_photos;

	function __construct ($name, $num_photos) {
		$this -> name = $name;
		$this -> num_photos = $num_photos;
	}
}

$domain = 'http://sony-asia-new.dev.interuptive.com/ilc/photographers/';
$countries = ['id/', 'my/', 'ph/', 'sea/', 'sg/', 'th/', 'vn/'];
$languages = ['', 'thai/', 'viet/'];

$photographers = [];
array_push($photographers, new Photographer ('benny', 12));
array_push($photographers, new Photographer ('bryan', 6));
array_push($photographers, new Photographer ('chester', 6));
array_push($photographers, new Photographer ('darren', 6));
array_push($photographers, new Photographer ('edward', 6));
array_push($photographers, new Photographer ('eric', 6));
array_push($photographers, new Photographer ('fakrul', 20));
array_push($photographers, new Photographer ('glenn', 5));
array_push($photographers, new Photographer ('kelvin', 6));
array_push($photographers, new Photographer ('kriengkrai', 4));
array_push($photographers, new Photographer ('manny', 9));
array_push($photographers, new Photographer ('nguyen', 4));
array_push($photographers, new Photographer ('nick', 13));
array_push($photographers, new Photographer ('nicoline', 6));
array_push($photographers, new Photographer ('pilar', 8));
array_push($photographers, new Photographer ('tavepong', 20));
array_push($photographers, new Photographer ('urasha', 7));

$space = "\r\n<br />";

$count = 0;

echo $domain . $space;
$count ++;

foreach ($countries as $country) {
	echo $domain . $country . $space;
	$count ++;

	foreach ($languages as $language) {

		if ($language !== '') {
			echo $domain . $country . $language . $space;
			$count ++;
		}

		foreach ($photographers as $photographer) {

			for ($i = 1; $i <= $photographer -> num_photos; $i ++) {
				echo $domain . $country . $language . 'photos/photo-' . $photographer -> name . '-' . $i . '.html' . $space;
				$count ++;
			}
		}
	}
}

echo 'Total number of links: ' . $count;

?>