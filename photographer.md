Photographer Landing Page
========================  

# Country Selector  


## To Add New country at `template/splash.html`  

~~~~
<div class="col-sm-4">
    <a href="th/thai/" onclick="var e = arguments[0] || window.event; if (e.stopPropagation != null) e.stopPropagation(); else e.cancelBubble = true; trackMs_link('ms:ilc:landing:country-selector:TH', 'ilc', 'ilc:landing:country-selector:TH', 'microsite|ilc|landing|country-selector|TH');" data-country="th">THAILAND</a>
    <!-- Add New Country Here -->
</div>
~~~~

~~~~
<select id="country-select">
	<option value="" selected disabled>MAKE YOUR SELECTION</option>
	<!-- Add New Country Here -->
</select>
~~~~  
  


## To add new KOL photo on the mobile randomize background 

Images is located at `img/splash/`, simply add one and rename it accordingly (i.e. bg-19.jpg).  
  

To add new background at `less/styles.less`
~~~~
.bg-18 {
	background-image: url(../img/splash/bg-18.jpg);
}
~~~~  
  
  
To add more grid at `template/splash.html`
~~~~
 <div class="body-container">
    <div class="boxes">
        <div class="col-sm-4 col-xs-4 bg-10"></div>
        <div class="col-sm-4 col-xs-4 bg-11"></div>
        <div class="col-sm-4 col-xs-4 bg-12"></div>
    </div>
</div>
~~~~  
  


- - - -  
  

# Homepage

## Image  

Images is located at `img/home/`


## To add new KOL at `template/content-index.html`  

* Update photographer-`th`-`vin` with the photographer country and name.  
* Update data-country="`th`" to the photographer's country.
* Update the onclick tracking with the KOL name (i.e. trackMs_link('ms:ilc:@@country:home:`bryan`', 'ilc:@@country', 'ilc:@@country:home:`bryan`', 'microsite|ilc:@@country|home|`bryan`'))


***Take note that each row, there are slight different in the coding format.***  

### The first-row template, there's an additional code as shown below
    <!-- first-row additional code-->
    <div class="content-header-bg hidden-sm hidden-xs"></div>

~~~~
<div id="photographer-id-benny" class="photographer col-sm-12 col-md-2" data-country="id">
    <a href="@@country/profile-benny.html" onclick="var e = arguments[0] || window.event; if (e.stopPropagation != null) e.stopPropagation(); else e.cancelBubble = true; trackMs_link('ms:ilc:@@country:home:benny', 'ilc:@@country', 'ilc:@@country:home:benny', 'microsite|ilc:@@country|home|benny');">
        <div class="content-header-bg hidden-sm hidden-xs"></div>
        <div class="bg desktop hidden-xs hidden-sm"></div>
        <div class="bg mobile hidden-md hidden-lg"></div>
        <div class="name text-center">
            <div class="hover">
                <p class="genre">Wedding Photographer</p>
            </div>
            <p class="photographer-name">Benny Lim</p>
            <p class="country-name">Indonesia</p>
        </div>
    </a>
</div>
~~~~


### The last-row template, there's an additional code as shown below
    <!-- last-row additional code-->
    <div class="content-footer-bg hidden-sm hidden-xs"></div>  

~~~~
<div id="photographer-my-nick" class="photographer col-sm-12 col-md-2" data-country="my">
    <a href="@@country/profile-nick.html" onclick="var e = arguments[0] || window.event; if (e.stopPropagation != null) e.stopPropagation(); else e.cancelBubble = true; trackMs_link('ms:ilc:@@country:home:nick', 'ilc:@@country', 'ilc:@@country:home:nick', 'microsite|ilc:@@country|home|nick');">
        <div class="bg desktop hidden-xs hidden-sm"></div>
        <div class="bg mobile hidden-md hidden-lg"></div>
        <div class="name text-center">
            <div class="hover">
                <p class="genre">Travel + Documentary Photographer</p>
            </div>
            <p class="photographer-name">Nick Ng</p>
            <p class="country-name">Malaysia</p>
        </div>
    </a>
    <div class="content-footer-bg hidden-sm hidden-xs"></div>
</div>
~~~~
  


### To add new KOL at `js/main.js` (Backstretch and image load)
~~~~
images.push('img/home/th/urasha.jpg');
~~~~

~~~~
images.push('img/home/th/urasha-mobile.jpg');
~~~~

~~~~
function desktopBg() {
    $('#photographer-id-benny .bg.desktop').backstretch('img/home/id/benny.jpg');
}
~~~~

~~~~
function mobileBg() {
    $('#photographer-id-benny .bg.mobile').backstretch('img/home/id/benny-mobile.jpg');
}
~~~~
