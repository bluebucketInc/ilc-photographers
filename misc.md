Misc
========================  


# Facebook Share Image

Edit Facebook share image path at index.template.html or index.template.html in any country folder (1.e. `id/index.template.html`)

~~~~
"fbImage": "##### domain #####img/fb_share.jpg",
~~~~
  

## Replacing Facebook Share Image  
  
* Open `FacebookDebugger.php` Comment or uncomment the line `staging` or `live` respectively.
~~~~
	//! staging
	$post = 'id=' . urlencode ($url) . '&scrape=true&access_token=858204864266067|rSm6TdHvRRWCUWjkgfuOPwjW8ww';

	//! live
	$post = 'id=' . urlencode ($url) . '&scrape=true&access_token=CAAMMiApGVz8BAHksah4c5ZCfeVsr8PZBZBStz9sCZBD3u8kZBX01Bsqxrfg57q3kQtKb4eBk4AigJQsEEAHS7FZCkdyuuoV8mihmNiRd21oIbtRyGFG37hzTS0e5neSvb0WB8DsZAGZCFkiy1sa9GyUkeuwWtzhIYY5B7Fa0ak6dYBYfHGx4IGlaxO1VqFVMUyOMVloBFF6nMKJydqPdOCkyKJZC7EahynosZD';
~~~~
* Open `rescrapeFB.php`. Comment or uncomment the domain line `staging` or `live` respectively.
~~~~
	$domain = 'http://sony-asia-new.dev.interuptive.com/ilc/photographers/';
	// $domain = 'http://www.sony-asia.com/microsite/ilc/photographers/';
~~~~
* Add/remove photographer or update the numbers of the photos.
~~~~
	<!-- example -->
	array_push($photographers, new Photographer ('joyce', 10));
~~~~
* Open terminal and run `php rescapeFB.php`.


-----------------------


# Tracking Code  

There are 3 types of tracking code, please take note whenever you creating a new page or adding a new link.

1. ***omniture tracking*** - place at the bottom of every page.  
2. ***cid*** - to be added behind a URL
	- (i.e. https://www.worldphoto.org/sony-world-photography-awards`?cid=dealers:alpha_professionals_sg:redirect`)  
3. ***onclick*** - to be added inside an anchor link
	- (i.e. trackMs_link('ms:ilc:@@country:a7Series:cameras:brochure-download', 'ilc:@@country', 'ilc:@@country:a7Series:cameras:brochure-download', 'microsite|ilc:@@country|a7Series|cameras|brochure-download')`)  


-----------------------  


