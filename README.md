# Setup (for Mac)

## Installation of homebrew

	ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

## Installation of node and npm

	brew install node

## Installation of bower

	npm install -g bower

## Installation of dependencies

Open the project directory at root level in terminal and run the following commands:

	bower install & npm install &

## Installation of gulp

	npm install -g gulp

## Updating of dependencies

	bower update & npm update &  





----------------------





# Development

## Compilation

Run `gulp build` to build/compile the files. The deployable folder is *dist*.

Run `gulp clean` to wipe the *dist* folder.

Run `gulp` to run `gulp clean` then `gulp build`.  



**Compression and Minifying**

Run `--compress` as a parameter to compress/minify the js and css files (i.e. `gulp --compress`).  



**Compression 2 Production Build commits**

Run `git diff --name-only <commit> <commit> >> diff.txt`, replacing the 2 `<commit>` with 2 different production build commits to compare the difference and output the files affected in a text file.




### Local  

***Run `gulp server` to start local server.*** Whenever there are changes made to the following, the browser will reload on its own and re-build/re-compile the files:

* all *.js* files in the *js* folder and subfolders
* all *.less* files in the *less* folder and subfolders
* all *.jpg* and *.png* in the *img* folder and subfolders
* all *.template.html* files in the root and countries folders
* all *.html* files in the *template* folder

Run `--development` as a parameter to only build the Singapore's batch of content, to reduce gulp's build time (i.e. `gulp server --development`).



### Staging

Run `--staging` as a parameter to compile the codes (i.e. `gulp --staging`). Use for staging.

***Run `gulp --staging --compress`*** to compile the codes and compress/minify the js and css files.



### Production

Run `--production` as a parameter to compile the codes (i.e. `gulp --production`). Use for production.

***Run `gulp --production --compress`*** to compile the codes and compress/minify the js and css files.

There is 1 file that require editing (FB App ID) before compiling for production:

 * #### *js/helper.js*
 	- Comment away `'fbId': '771158533004598' //! staging` and enable `'fbId': '771157943004657' //! live`.  

NOTE: Add manual meta-refresh for ID Page


----------------------  



# Files Packing for Client

Create a folder name `photographers`, pack affected files from `dist` folder and added into this folder.  



----------------------  


# Developing  


## Styling

All styling are to be done in the `less` folder in `.less` format.  


## Plugins  

Click [Plugins](https://bitbucket.org/interuptive/sony-ap-alpha-kol-microsite/src/master/plugins.md) to view all the plugins used.  



----------------------



## Live

Link: ***[http://www.sony-asia.com/microsite/ilc/photographers/](http://www.sony-asia.com/microsite/ilc/photographers/)***





----------------------  
